An interactive git viewer based on urwid.
Similar to gitk but in a terminal.

Please see the [README](https://gitlab.com/erzo/git-viewer/-/blob/master/README.md) on GitLab.

The change log is in the [tag descriptions](https://gitlab.com/erzo/git-viewer/-/tags).
